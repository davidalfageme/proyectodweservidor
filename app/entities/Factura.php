<?php

namespace ticket\app\entities;

class Factura{
    private $id;
    private $cantidadEntradas;
    private $comprador;
    private $precioTotal;
    private $descuento;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCantidadEntradas()
    {
        return $this->cantidadEntradas;
    }

    /**
     * @param mixed $cantidadEntradas
     */
    public function setCantidadEntradas($cantidadEntradas)
    {
        $this->cantidadEntradas = $cantidadEntradas;
    }

    /**
     * @return mixed
     */
    public function getComprador()
    {
        return $this->comprador;
    }

    /**
     * @param mixed $comprador
     */
    public function setComprador($comprador)
    {
        $this->comprador = $comprador;
    }

    /**
     * @return mixed
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    /**
     * @param mixed $precioTotal
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;
    }

    /**
     * @return mixed
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param mixed $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }


}
