<?php

namespace ticket\app\entities;

use ticket\app\helpers\MomentPHP;

class Entrada{
    private $id;
    private $codigo;
    private $evento;
    private $factura;
    private $precioVenta;
    private $precio;
    private $nombre;
    private $fecha;
    private $imagen;
    private $gradient1;
    private $gradient2;
    private $direccion;
    private $cantidadEntradas;

    /**
     * @return mixed
     */
    public function getCantidadEntradas()
    {
        return $this->cantidadEntradas;
    }

    /**
     * @param mixed $cantidadEntradas
     */
    public function setCantidadEntradas($cantidadEntradas): void
    {
        $this->cantidadEntradas = $cantidadEntradas;
    }


    public function getDifFecha(){

    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen): void
    {
        $this->imagen = $imagen;
    }



    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getFecha(bool $dif = false)
    {
        if($dif){
            $fechaEvento = new MomentPHP($this->fecha);
            $ahora = new MomentPHP();

            return $ahora->from($fechaEvento);
        }else{
            return $this->fecha;
        }
    }

    /**
     * @return mixed
     */

    public function getGradientStyle(){
        $grad1RGB = implode(', ' , $this->getRGBfromHex($this->getGradient1()));
        $grad2RGB = implode(',',$this->getRGBfromHex($this->getGradient2()));

        return "background: linear-gradient(40deg,rgba($grad1RGB,0.8),rgba($grad2RGB, 0.8));";
    }


    private function getRGBfromHex(string $hex){
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return explode(' ', "$r $g $b");
    }



    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }


    /**
     * Entrada constructor.
     * @param $id
     * @param $codigo
     * @param $evento
     * @param $factura
     * @param $precioVenta
     * @param $precio
     */
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @param mixed $evento
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;
    }

    /**
     * @return mixed
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * @param mixed $factura
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;
    }

    /**
     * @return mixed
     */
    public function getPrecioVenta()
    {
        return $this->precioVenta;
    }

    /**
     * @param mixed $precioVenta
     */
    public function setPrecioVenta($precioVenta)
    {
        $this->precioVenta = $precioVenta;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getGradient1()
    {
        return $this->gradient1;
    }

    /**
     * @param mixed $gradient1
     */
    public function setGradient1($gradient1): void
    {
        $this->gradient1 = $gradient1;
    }

    /**
     * @return mixed
     */
    public function getGradient2()
    {
        return $this->gradient2;
    }

    /**
     * @param mixed $gradient2
     */
    public function setGradient2($gradient2): void
    {
        $this->gradient2 = $gradient2;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    public function getHora(){
        return date('H:i',strtotime($this->getFecha()));
    }

}
