<?php

namespace ticket\app\entities;
use ticket\app\helpers\MomentPHP;
use ticket\app\helpers\SubirFichero;

class Evento{

    use SubirFichero;

    private $id;
    private $nombre;
    private $descripcion;
    private $imagen;
    private $gradient1;
    private $gradient2;
    private $fecha;
    private $precio;
    private $ciudad;
    private $direccion;
    private $categoria;
    private $entradasTotales;
    private $entradasDisponibles;
    private $creador;
    private $inicioVentaEntradas;
    private $finVentaEntradas;
    private $slider;



    /**
     * @return mixed
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * @param mixed $slider
     */
    public function setSlider($slider): void
    {
        $this->slider = $slider;
    }


    /**
     * @return mixed
     */
    public function getInicioVentaEntradas()
    {
        return $this->inicioVentaEntradas;
    }

    /**
     * @param mixed $inicioVentaEntradas
     */
    public function setInicioVentaEntradas($inicioVentaEntradas): void
    {
        $this->inicioVentaEntradas = $inicioVentaEntradas;
    }

    /**
     * @return mixed
     */
    public function getFinVentaEntradas()
    {
        return $this->finVentaEntradas;
    }

    /**
     * @param mixed $finVentaEntradas
     */
    public function setFinVentaEntradas($finVentaEntradas): void
    {
        $this->finVentaEntradas = $finVentaEntradas;
    }

    public function getInfoFechasVenta(){
        $fechaActual = new MomentPHP();
        $fechaFin = new MomentPHP($this->getFinVentaEntradas());
        $fechaInicio = new MomentPHP($this->getInicioVentaEntradas());

        if($fechaInicio>$fechaActual){
            return 'La venta de entradas comienza ' . $fechaActual->from($fechaInicio);
        }else if($fechaFin>$fechaActual){
            return _('Te quedan ' . str_replace('en ', '', $fechaActual->from($fechaFin)) .' para comprar entradas');
        }else{
            return 'Ya no se pueden comprar entradas para este evento';
        }

        $ahora = new MomentPHP();

        return $ahora->from($fechaEvento);
    }

    public function creaConPost()
    {
        $this->setNombre($_POST['nombre']);
        $this->setDescripcion($_POST['descripcion']);
        $this->setFecha($_POST['fecha']);
        $this->setPrecio($_POST['precio']);
        $this->setCiudad($_POST['ciudad']);
        $this->setDireccion($_POST['direccion']);
        $this->setCategoria($_POST['categoria']);
        $this->setEntradasTotales($_POST['entradasTotales']);
        $this->setGradient1((!isset($_POST['gradient1']) || is_null($_POST['gradient1'])) ? '' : $_POST['gradient1']);
        $this->setGradient2((!isset($_POST['gradient2']) || is_null($_POST['gradient2'])) ? '' : $_POST['gradient2']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return date('jS F',strtotime($this->fecha));
    }


    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = +$precio;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getEntradasTotales()
    {
        return $this->entradasTotales;
    }

    /**
     * @param mixed $entradasTotales
     */
    public function setEntradasTotales($entradasTotales)
    {
        $this->entradasTotales = $entradasTotales;
    }

    /**
     * @return mixed
     */
    public function getEntradasDisponibles()
    {
        return $this->entradasDisponibles;
    }

    /**
     * @param mixed $entradasDisponibles
     */
    public function setEntradasDisponibles($entradasDisponibles)
    {
        $this->entradasDisponibles = $entradasDisponibles;
    }

    /**
     * @return mixed
     */
    public function getCreador()
    {
        return $this->creador;
    }

    /**
     * @param mixed $creador
     */
    public function setCreador($creador)
    {
        $this->creador = $creador;
    }


    /**
     * @return mixed
     */
    public function getGradient1()
    {
        return $this->gradient1;
    }

    /**
     * @param mixed $gradient1
     */
    public function setGradient1($gradient1)
    {
        $this->gradient1 = $gradient1;
    }

    /**
     * @return mixed
     */
    public function getGradient2()
    {
        return $this->gradient2;
    }

    /**
     * @param mixed $gradient2
     */
    public function setGradient2($gradient2)
    {
        $this->gradient2 = $gradient2;
    }

    public function toString(){
        return $this->getNombre() . "  PRECIO: " . $this->getPrecio() . " CIUDAD: " . $this->getCiudad();
    }

    public function getGradientStyle(){
        $grad1RGB = implode(', ' , $this->getRGBfromHex($this->getGradient1()));
        $grad2RGB = implode(',',$this->getRGBfromHex($this->getGradient2()));

        return "background: linear-gradient(40deg,rgba($grad1RGB,0.8),rgba($grad2RGB, 0.8));";
    }

    private function getRGBfromHex(string $hex){
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return explode(' ', "$r $g $b");
    }



}