<?php
/**
 * @var \ticket\core\Router $router
 */


$router->get('caracola', 'UsuarioController@nuevoUsuario');

$router->get('about', 'controllers/about.php');

$router->get(
    'contactos/nuevo', 'controllers/form-nuevo.php');

$router->post(
    'contactos/nuevo',
    'controllers/nuevo-contacto.php');


//Routas de evento
$router->get('', 'EventoController@listar', 'anonimo');
$router->post('eventos/nuevo', 'EventoController@crear', 'gestor');
$router->get( 'eventos/nuevo', 'EventoController@formNuevo', 'gestor');
$router->get('eventos', 'EventoController@listar', 'anonimo');
$router->get('eventos/:id', 'EventoController@mostrar', 'anonimo');

//Rutas de usuario
$router->get('login', 'AuthController@mostrar', 'anonimo');
$router->post('login', 'AuthController@checkLogin', 'anonimo');
$router->get('logout', 'AuthController@logout');
$router->get('registro', 'UsuarioController@mostrar', 'anonimo');
$router->post('registro', 'UsuarioController@crearUsuario');
$router->get('perfil', 'UsuarioController@mostrarPerfil', 'comprador');
$router->get('perfil/:id', 'UsuarioController@mostrarPerfil', 'comprador');
$router->get('usuarios/:pagina', 'UsuarioController@listarUsuarios', 'comprador');

//Rutas de entradas
$router->get('entradas', 'EntradaController@mostrar', 'comprador');
$router->get('entradas/comprar', 'EntradaController@comprarEntrada', 'comprador');
$router->post('entradas/comprar', 'EntradaController@confirmarCompra', 'comprador');
$router->get('entradas/qr/:codigo', 'EntradaController@getQr', 'comprador');

//Rutas de mensaje
$router->get('mensajes', 'MensajeController@mostrarConversaciones', 'comprador');
$router->post('mensajes/enviar', 'MensajeController@enviarMensaje', 'comprador');
$router->get('mensajes/:idUsuario', 'MensajeController@mostrarMensajes', 'comprador');
