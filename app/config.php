<?php

return array(
    'database' => array(
        'name' => 'ticket',
        'username' => 'ticket',
        'password' => 'uiY8FAR0rZhV0R0h',
        'connection' => 'mysql:host=localhost',
        'options' => array(
            PDO:: MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO:: ATTR_ERRMODE => PDO:: ERRMODE_EXCEPTION ,
            PDO:: ATTR_PERSISTENT => true
        )
    ),
    'logs' => array(
        'name' => 'Registro Error',
        'file' => '../logs/ticket-error.log'
    ),
    'security' => array(
        'roles' => array(
            'admin'=>3,
            'gestor'=>2,
            'comprador'=>1,
            'anonimo'=>0
        )
    ),
    'idiomasDisponibles' => array(
        'es_ES.utf8',
        'en_GB.utf8'
    )
);