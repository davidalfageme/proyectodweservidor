<?php
/** @var $usuarioMostrar ticket\app\entities\usuario */
?>

<section id="perfilUsuario">
    <div id="cabecera">
        <img src="/uploads/usuarios/<?=$usuarioMostrar->getAvatar()?>" alt="">
        <h2><?= $usuarioMostrar->getNombre()?></h2>
    </div>

    <div id="datos">
        <h2>Acciones</h2>
        <a href="/entradas" id="enlaceEntradas" class="accion">
            <i class="fa fa-ticket" aria-hidden="true"></i>
            <p>Ver mis entradas</p>
        </a>
        <a href="/usuario/cambiarPassword" id="enlaceEntradas" class="accion">
            <i class="fa fa-asterisk" aria-hidden="true"></i>
            <p>Cambiar contraseña</p>
        </a>

        <h2>Datos</h2>
        <article>
             <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p><?= $usuarioMostrar->getCiudad()?></p>
        </article>
        <article>
            <i class="fa fa-certificate" aria-hidden="true"></i>
            <p><?= $usuarioMostrar->getRol()?></p>
        </article>
        <article>
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <p><?= $usuarioMostrar->getEmail()?></p>
        </article>


    </div>
</section>
