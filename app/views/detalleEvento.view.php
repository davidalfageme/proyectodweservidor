<?php/** @var $evento ticket\app\entities\evento */?>

<section id="detalleEvento">
    <div id="imagen">
        <img src="/uploads/eventos/<?= $evento->getImagen()?>">

    </div>
    <div id="contenido">
        <h1><?= $evento->getNombre()?></h1>
        <div id="datos">
            <div id="ciudad">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <p><?= $evento->getCiudad();?></p>
            </div>
            <div id="fechaEvento">
                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                <p><?= $evento->getFecha() ?></p>
            </div>
            <div id="precio">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <p><?= $evento->getPrecio(); ?>€</p>
            </div>

        </div>
        <div id="entradas">
            <a href="/entradas/comprar?evento=4" class="button">comprar</a>
            <p id="entradasDisponibles">
                <?php
                $entradasDisponibles = $evento->getEntradasDisponibles();
                $entradasTotales = $evento->getEntradasTotales();
                printf(
                    ngettext('Queda <b>%d </b> de %d entrada', 'Quedan <b>%d</b> de %d entradas ', $entradasDisponibles),
                    $entradasDisponibles, $entradasTotales) ?>
            </p>
            <p id="infoVentaEntradas"><?=$evento->getInfoFechasVenta()?></p>
        </div>
        <p id="tituloCreador">Creador del evento</p>
        <div id="creador">
            <a href="/eventos?usuario=<?=$creador->getId()?>" class="avatar">
                <img  src="/uploads/usuarios/<?= $creador->getAvatar()?>" alt="<?= $creador->getNombre()?>">
                <h2><?=$creador->getNombre()?></h2>
            </a>
            <div class="texto">
            </div>
            <a href="/perfil/<?=$creador->getId()?>" >
                <i class="fa fa-user" aria-hidden="true"></i>
                <p><?= _('Perfil');?></p>
            </a>
            <a href="/mensajes/<?=$creador->getId()?>" >
                <i class="fa fa-commenting" aria-hidden="true"></i>
                <p><?= _('Mensaje');?></p>
            </a>

        </div>
        <div id="descripcion">
            <?= $evento->getDescripcion(); ?>
        </div>
    </div>
</section>
