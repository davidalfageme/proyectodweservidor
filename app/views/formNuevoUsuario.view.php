<script src='https://www.google.com/recaptcha/api.js'></script>
<h1> <?= _('Registro')?></h1>
<h2><?= _('!Vamos a crear una cuenta!')?></h2>
<?php if (isset($error)): ?>
    <div class="error">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <p><?= $error ?></p>
    </div>
<?php endif; ?>
<form action="/registro" method="post" id="formularioNuevoUsuario" enctype="multipart/form-data">
    <label for="nombre">Nombre:</label>
    <input type="text" name="nombre" id="nombre">
    <label for="apellidos">Apellidos</label>
    <input type="text" name="apellidos" id="apellidos">
    <label for="email">Email:</label>
    <input type="email" name="email" id="email">
    <label for="password">Contraseña</label>
    <input type="password" name="password" id="passowrd">
    <label for="password">Confirmar contraseña</label>
    <input type="confirmarPassword"  id="passowrdConfirm">
    <label for="direccion">Dirección:</label>
    <input type="text" name="direccion" id="direccion">
    <label for="ciudad">Ciudad:</label>
    <input type="text" name="ciudad" id="ciudad">
    <label for="avatar">Imagen de usuario</label>
    <input type="file" name="avatar" id="avatar">

    <div class="g-recaptcha" data-sitekey="6LfEfD8UAAAAAERBGHmO_rexnIzvjIoTePjYe6iJ"></div>
    <input type="submit" value="ENVIAR">
</form>