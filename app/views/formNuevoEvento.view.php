<script src="/js/jscolor.min.js"></script>
<script>
     window.addEventListener('load', ()=>{
         let bindIds = ["nombre", "descripcionEvento", "fecha", "ciudad", "precio", "entradas", "degradado1", "degradado2"];
         bindEvent(bindIds);
         document.getElementById('imagen').addEventListener('change', setImage);
         let divCategorias = document.getElementById('categorias');
         divCategorias.addEventListener('scroll', () =>{
             divCategorias.classList.remove('incitateScroll');
         });
     })
</script>
<div id="nuevoEvento">

    <form action="/eventos/nuevo"
          method="post" enctype="multipart/form-data" id="formNuevoEvento">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" class="nombre">
        </div>
        <div>
            <label for="descripcion">Descripción</label>
            <textarea type="text" name="descripcion" id="descripcionEvento">

        </textarea>
        </div>

        <div>
            <label for="fecha">Fecha:</label>
            <input type="date" name="fecha" id="fecha">
        </div>
        <div>
            <label for="ciudad">Ciudad:</label>
            <input type="text" name="ciudad" id="ciudad">
        </div>
        <div>
            <label for="direccion">Direccion:</label>
            <input type="text" name="direccion" id="direccion">
        </div>

        <label for="categoria" >Categoria</label>
        <div  id="categorias" class="incitateScroll">
            <!--        <select name="categoria" id="categoria">-->
            <?php foreach($categorias as $categoria) : ?>

                <label class="contenedorCategoria">
                    <input type="radio" name="categoria" value="<?=$categoria->getId();?>">
                    <span class="checkmark"><i class="fa <?=$categoria->getIcon();?>" aria-hidden="true"></i> <?=$categoria->getNombre();?></span>
                </label>
            <?php endforeach; ?>
            <!--        </select>-->
        </div>
        <div>
            <label for="precio">Precio:</label>
            <input type="number" name="precio" id="precio">
        </div>

        <div>
            <label for="entradas">Entradas:</label>
            <input type="number" name="entradasTotales" id="entradas">
        </div>

        <div>
            <label for="imagen">Imagen:</label>
            <input type="file" name="imagen" id="imagen">
        </div>

        <div>
            <label for="gradient1">Degradado</label>
            <div id="degradados">
                <input type="text" class="jscolor" value="4759F6" name="gradient1">
                <input type="text" class="jscolor" value="5C258D" name="gradient2">
            </div>
        </div>

        <input type="submit" name="enviar" value="Crear evento">
    </form>

    <div id="previewEvento">
        <h2>Preview evento</h2>
        <section id="detalleEvento">
            <div id="imagen">
                <img src="http://via.placeholder.com/768x420" id="previewimagen">
            </div>
            <div id="contenido">
                <h1 id="previewnombre"></h1>
                <div id="datos">
                    <div id="ciudad">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <p id="previewciudad"></p>
                    </div>
                    <div id="fechaEvento">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <p id="previewfecha"></p>
                    </div>
                    <div id="precio">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                        <p ><span id="previewprecio"></span>€</p>
                    </div>
                </div>
                <div id="entradas">
                    <button type="submit">comprar</button>
                    <p id="entradasDisponibles">
                        Quedan <b><span id="previewentradas"></span></b> entradas
                    </p>
                </div>
                <div id="descripcion">
                    <span id="previewdescripcionEvento"></span>
                </div>
            </div>
        </section>
    </div>
</div>