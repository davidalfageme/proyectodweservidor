<?php
/** @var $usuario ticket\app\entities\usuario */
?>
<ul id="usuarios">
    <?php foreach ($usuarios as $usuario): ?>
        <li class="usuario">
            <img src="/uploads/usuarios/<?=$usuario->getAvatar()?>" alt="<?= _('Imagen de usuario de '. $usuario->getNombre());?>" class="avatar">
            <p class="nombre"><?=$usuario->getNombre();?></p>
            <p class="correo"><?=$usuario->getEmail()?></p>
            <p class="rol"><?=$usuario->getRol()?></p>
            <div class="acciones">
                <a href="/usuarios/editar/<?= $usuario->getId()?>" class="editar">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="/usuarios/eliminar/<?= $usuario->getId()?>" class="borrar">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </li>
    <?php endforeach;?>
</ul>

<script>
    window.addEventListener('load', ()=>{
      let ulsUsuario = document.getElementsByClassName('usuario');
      console.log(ulsUsuario);
      for(let i = 0; i < ulsUsuario.length; i++){
          let ulUsuario = ulsUsuario[i];
          ulUsuario.addEventListener('click', (el)=>{
              if(ulUsuario.classList.contains('seleccionado')){
                  ulUsuario.classList.remove('seleccionado');
              }else{
                  ulUsuario.classList.add('seleccionado');
              }
          })
      }
    })
</script>