<?php
    $usuario = \ticket\core\App::get('user');
    use ticket\core\Security;
?>
<header class="site-header">
    <a href="/" class="logo">TICKETS</a>
    <nav class="site-nav <?= is_null($usuario)?'notLogged':''?>">
        <ul>
            <li class="<?= $menuActivo === 'eventos'? 'active': ''?>"><a href="/eventos/"><i class="fa fa-star" aria-hidden="true"></i><?= _('Eventos')?></a></li>
            <?php if (Security::isUserGranted('admin')):?>
            <!-- ADMIN MENU -->
                <li class="<?= $menuActivo === 'usuarios'? 'active': ''?>"><a href="/usuarios/1"><i class="fa fa-users" aria-hidden="true"></i><?= _('Usuarios')?></a></li>
                <li class="<?= $menuActivo === 'mensajes'? 'active': ''?>"><a href="/mensajes/"><i class="fa fa-comments-o" aria-hidden="true"></i><?= _('Mensajes')?></a></li>
                <li class="<?= $menuActivo === 'misEventos'? 'active': ''?>"><a href="/eventos?usuario=<?=$usuario->getId()?>">
                        <i class="fa fa-bolt" aria-hidden="true"></i>
                        <?= _('Mis Eventos')?></a>
                <li class="<?= $menuActivo === 'entradas'? 'active': ''?>"><a href="/entradas/"><i class="fa fa-ticket" aria-hidden="true"></i><?= _('Entradas')?></a></li>
<!--                <li><a href="#0"><i class="fa fa-unlock-alt" aria-hidden="true"></i>Admin</a></li>-->
            <?php elseif(Security::isUserGranted('gestor')): ?>
            <!-- GESTOR MENU-->
                <li class="<?= $menuActivo === 'misEventos'? 'active': ''?>"><a href="/eventos?usuario=<?=$usuario->getId()?>">
                        <i class="fa fa-bolt" aria-hidden="true"></i>
                        <?= _('Mis Eventos')?></a>
                </li>
                <li class="<?= $menuActivo === 'entradas'? 'active': ''?>">
                    <a href="/entradas/"><i class="fa fa-ticket" aria-hidden="true"></i><?= _('Entradas')?></a></li>
                <li class="<?= $menuActivo === 'mensajes'? 'active': ''?>"><a href="/mensajes/">
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                        <?= _('Mensajes')?></a>
                </li>
<!--                <li ><a href="#0"><i class="fa fa-bar-chart" aria-hidden="true"></i>Datos</a></li>-->
            <?php elseif (Security::isUserGranted('comprador')):?>
            <!-- COMPRADOR MENU -->
                <li class="<?= $menuActivo === 'entradas'? 'active': ''?>"><a href="/entradas/"><i class="fa fa-ticket" aria-hidden="true"></i><?= _('Entradas')?></a></li>
                <li class="<?= $menuActivo === 'mensajes'? 'active': ''?>"><a href="/mensajes/"><i class="fa fa-comments-o" aria-hidden="true"></i><?= _('Mensajes')?></a></li>
            <?php endif?>
        </ul>
    </nav>
    <div class="account-actions">
        <?php if(!is_null($usuario)) : ?>
            <a href="/logout" class="sign-out-link" id="logout">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
                <span>Salir <span class="nombre">(<?= explode(' ',$usuario->getNombre())[0]?>)</span></span>
            </a>
        <?php endif; ?>
        <?php if(is_null($usuario)) : ?>
            <?php if ($menuActivo !== "login" && $menuActivo !== 'registro'): ?>
                <a href="/registro" class="sign-out-link" id="registrarse"><i class="fa fa-user-plus"></i><span><?= _('Registro')?></span></a>
                <a href="#" class="sign-out-link" id="iniciarSesion"><i class="fa fa-user-circle"></i><span><?= _('Entrar')?></span></a>
            <?php endif;?>
        <?php else: ?>
            <a href="/perfil" class="sign-out-link" id="miCuenta"><i class="fa fa-user-circle"></i><span>Mi perfil</span></a>
        <?php endif; ?>
    </div>
</header>