
<head>
    <meta charset="UTF-8">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,400,700,900" rel="stylesheet">


    <link rel="stylesheet" href="/estilos/fa-icons/css/font-awesome.min.css">
    <link rel="stylesheet" href="/estilos/main.css">
    <title><?= $tituloPagina ?></title>
    <script src="/js/index.js"></script>
    <meta name="theme-color" content="#0D1313">
</head>
