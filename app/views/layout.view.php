<!doctype html>
<html lang="<?=$_SESSION['language']?>">
    <?php
        include 'partials/head.php'
    ?>
<body>
    <span  id="webContent">
         <?php
         include 'partials/header.part.php';
         ?>

        <div id="main" class="<?= str_replace(' ', '', $tituloPagina)?> <?= is_null($usuario)?'notLogged':''?>">
            <?= $mainContent ?>
        </div>

        <?php
        include 'partials/footer.part.php';
        ?>
    </span>

    <?php if($tituloPagina !== "Entrar"): ?>
    <div id="modalLogin" class="hidden">
        <?php include 'login.view.php';?>
    </div>
    <?php endif; ?>

</body>
