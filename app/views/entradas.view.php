
<section id="misEntradas">
    <h1>Mis entradas</h1>
    <?php foreach ($entradas as $entrada):?>
        <article>
            <span class="visible">
                <a href="/eventos/<?= $entrada->getId()?>" class="enlaceEvento"><h2 class="nombreEvento"><?=$entrada->getNombre()?></h2></a>
                <p class="diasRestantes"><?=$entrada->getFecha(true) ;?></p>
                <img src="/uploads/eventos/<?= $entrada->getImagen() ;?>" class="imgEntrada">
                <span class="degradadoEntrada" style="<?=$entrada->getGradientStyle()?>"></span>
            </span>

            <span class="desplegable">
                <div class="direccion">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    <p> <?= $entrada->getDireccion()?></p>
                </div>
                <div class="fecha">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    <p> <?= date('d/m/y',strtotime($entrada->getFecha()))?></p>
                </div>
                <div class="hora">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <p> <?= $entrada->getHora()?></p>
                </div>
                <div class="numEntradas">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <p> <?= $entrada->getCantidadEntradas()?></p>
                </div>
                <div class="precio">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <p> <?= $entrada->getPrecio()?>€</p>
                </div>

                <a href="/entradas/qr/<?= $entrada->getCodigo(); ?>" target="blank" class="qr">
                    <i class="fa fa-qrcode" aria-hidden="true"></i>
                    <p> <?=_("Ver QR") ?></p>
                </a>
                <img class="qrEntrada" src="/entradas/qr/<?= $entrada->getCodigo(); ?>">
            </span>
        </article>
    <?php endforeach;?>
</section>

