<?php if (isset($error)): ?>
    <div class="error">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <p><?= $error ?></p>
    </div>
<?php endif; ?>


<form action="/login" method="post" enctype="multipart/form-data" id="login">
    <i id="closeModal" class="fa fa-times hidden" aria-hidden="true"></i>
    <label for="email">Email:</label>
    <input type="email" name="email" id="email">
    <label for="password">Contraseña:</label>
    <input type="password" name="password" id="password">
    <span id="loginActions">
        <input type="submit" value="ENTRAR">
        <a href="/registro" class="button orange">Registrate</a>
    </span>

</form>