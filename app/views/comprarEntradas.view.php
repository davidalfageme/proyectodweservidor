<?php
/**
 * Created by PhpStorm.
 * User: David Alfageme
 * Date: 07/01/2018
 * Time: 21:16
 */

?>
<div id="compraEntradas">
    <h1><?= _('Comprar entradas para ') . $evento->getNombre()?>  </h1>
    <h2><?= _('Precio por entrada:  ') . $evento->getPrecio()?> €</h2>
    <form action="/entradas/comprar/" method="post" >
        <label for="cantidad"></label>
        <input type="number" max="<?= $evento->getEntradasDisponibles()?>" value="2" name="cantidadEntradas" id="cantidadEntradas">
        <input type="number" name="evento" value="<?=$evento->getId()?>" class="hidden">
        <h2>Total: </h2>
        <input type="submit" value="COMPRAR">
    </form>
</div>

