<?php $usuario = \ticket\core\App::get('user'); ?>
<div id="mensajes">
    <div id="infoConversacion">
        <a href="/mensajes" class="volver"><i class="fa fa-angle-left"></i></a>
        <a href="#" class="avatar"><img src="/uploads/usuarios/<?=$contacto->getAvatar()?>" alt="" ></a>
        <h1><?= $contacto->getNombre()?></h1>
        
    </div>
    <ul id="historialMensajes">
        <?php foreach ($mensajes as $mensaje):?>
            <?php $enviadoPorMi = $mensaje['idEmisor']===$usuario->getId()?>
            <li class="mensaje <?= $enviadoPorMi?'enviadoPorMi':'enviadoPorContacto'?>" >
                <?= $mensaje['texto']?>
            </li>
        <?php endforeach;?>
    </ul>
    <form action="" id="enviarMensaje">
        <span id="groupTexto">
            <input type="text" name="texto" id="texto" required>
            <label for="texto" class="etiqueta"><i class="fa fa-commenting" aria-hidden="true"></i>Mensaje</label>
        </span>
        <input type="text" value="<?=$contacto->getId()?>" class="hidden" id="destinatario">
        <button id="enviar"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>

    </form>
</div>

<script src="/js/mensajes.js"></script>