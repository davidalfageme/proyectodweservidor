<?php if($esBusqueda === false): ?>
    <h2 id="tituloDestacados"><i class="fa fa-rocket" aria-hidden="true"></i> <?= _("Eventos destacados")?></h2>
    <section id="slider">
        <?php foreach($destacados as $evento):?>
            <article class="slide">
                <span class="gradientSlider" style="<?= $evento->getGradientStyle();?>"></span>
                <img class="imageSlider" src="/uploads/eventos/<?=$evento->getImagen();?>" alt="">
                <h2 class="tituloSlider"><?=$evento->getNombre()?></h2>
            </article>
        <?php endforeach;?>
    </section>
<?php endif; ?>


<div class="contenedorEventos">
    <?php if(count($eventos)===0): ?>
        <!--no hay eventos-->
        <div id="notFound">
            <i class="fa fa-frown-o" aria-hidden="true"></i>
            <h1><?= _('No se encontraron eventos para su busqueda');?></h1>
        </div>

    <?php endif; ?>
    <?php if(\ticket\core\Security::isUserGranted('gestor') && !$esBusqueda):?>
        <h2 id="tituloNuevoEvento"><i class="fa fa-plus-circle fa-fw" aria-hidden="true"></i><?= _('Crear nuevo evento')?></h2>
        <a href="/eventos/nuevo" id="cardNuevoEvento">
            <i class="fa fa-plus"></i>
            <p>Nuevo evento</p>
        </a>
    <?php endif;?>

    <h2 id="tituloEventos"><i class="fa fa-star" aria-hidden="true"></i><?= _('Eventos')?></h2>
    <?php foreach($eventos as $evento) : ?>
        <article class="evento tarjeta">
            <div class="imagenTarjeta">
                <img src="/uploads/eventos/<?=$evento->getImagen();?>" alt="">
            </div>
            <div class="tituloTarjeta" style="<?= $evento->getGradientStyle();?>">
                <h2 ><?=$evento->getNombre();?></h2>
<!--                <a href="#" class="tituloTarjeta"></a>-->
            </div>
            <a href="<?= "/eventos/". $evento->getId();?>" class="cuerpoTarjeta">
                <p class="descripcion">
                    <?php echo substr($evento->getDescripcion(), 0, 60);
                    if(strlen($evento->getDescripcion()) > 250 ) {

//                        echo " <a href='".$evento->getId()."'>...</a>";
                    }
                    ?>
                </p>
                <div class="masDetalles">
                    <p class="ciudad">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <?= $evento->getCiudad(); ?>
                    </p>
                    <div class="fechaEvento">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <?= $evento->getFecha() ?>
                    </div>
                    <div class="precio">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                        <h3 class="price"><?= $evento->getPrecio(); ?>€</h3>
                    </div>
                </div>
            </a>
        </article>
    <?php endforeach; ?>
</div>

<p id="contadorEventos">
    <?php
    $numEventos = count($eventos);
    printf(
        ngettext(
            "Mostrando %d evento",
            "Mostrando %d eventos",
            $numEventos),
        $numEventos);
    ?>
</p>

<div id="busqueda">
    <div id="iconoBusqueda" class="inicioPagina">
        <i class="fa fa-search inicioPagina" aria-hidden="true"></i>
        <p>Buscar</p>
    </div>
    <span id="backgroundBusqueda"></span>
    <span id="backgroundWeb"></span>

    <i class="fa fa-close" aria-hidden="true" id="iconoCerrar"></i>
    <form name="buscarEventos" id="buscarEventos" action="/eventos">
        <input type="text" id="textoBusqueda" name="textoBusqueda" placeholder="Busqueda">
        <div  id="categorias">
            <?php foreach($categorias as $categoria) : ?>

                <label class="contenedorCategoria">
                    <input type="checkbox" name="categorias[]" value="<?=$categoria->getId();?>">
                    <span class="checkmark"><i class="fa <?=$categoria->getIcon();?>" aria-hidden="true"></i> <?=$categoria->getNombre();?></span>
                </label>
            <?php endforeach; ?>
        </div>
        <input type="submit" value="Buscar" id="submitBusqueda">
    </form>

</div>

<script>
    window.addEventListener('load', ()=>{
        let busqueda = document.getElementById('busqueda');
        let iconoBusqueda = document.getElementById('iconoBusqueda');
        let backgroundWeb = document.getElementById('backgroundWeb');
        let iconoCerrar = document.getElementById('iconoCerrar');
        let formularioBusqueda = document.getElementById('buscarEventos');

        let toggleBusqueda = function(){
            if(busqueda.classList.contains('busquedaActiva')){
                iconoBusqueda.classList.remove('inicioPagina');
                iconoBusqueda.classList.add('cerrarBusqueda');
                busqueda.classList.remove('busquedaActiva');
            }else{
                busqueda.classList.add('busquedaActiva');
            }
        }

        let searchIconAction = function(){
            if(busqueda.classList.contains('busquedaActiva')){
                busqueda.classList.remove('busquedaActiva');
                formularioBusqueda.submit();
            }else{
                busqueda.classList.add('busquedaActiva');
            }
        }

        iconoBusqueda.addEventListener('click', searchIconAction);
        backgroundWeb.addEventListener('click', toggleBusqueda);
        iconoCerrar.addEventListener('click', toggleBusqueda);


    });
</script>