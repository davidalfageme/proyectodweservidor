<?php $usuario = \ticket\core\App::get('user'); ?>
<?php use ticket\app\helpers\MomentPHP?>
<?//= var_dump($mensajes)?>
<div id="conversaciones">
    <ul id="listaConversaciones">
        <?php foreach($conversaciones as $conversacion):?>
            <?php $enviadoPorMi = $conversacion['idEmisor']===$usuario->getId()?>
            <li class="conversacion <?=$enviadoPorMi?'enviado':'recivido'?>">
                <a href="/mensajes/<?=$enviadoPorMi?$conversacion['idReceptor']:$conversacion['idEmisor']?>">
                    <img src="/uploads/usuarios/<?=$enviadoPorMi?$conversacion['avatarReceptor']:$conversacion['avatarEmisor']?>" alt="" class="avatar">
                    <?php if($conversacion['leido'] == 0 && !$enviadoPorMi): ?>
                        <i class="fa fa-bell sinLeer" aria-hidden="true"></i>
                    <?php endif;?>
                    <?php if($conversacion['leido'] == 1 && $enviadoPorMi): ?>
                        <i class="fa fa-eye leidoDestinatario" aria-hidden="true"></i>
                    <?php endif;?>
                    <div class="datos">
                        <h3 class="nombre"><?=$enviadoPorMi?$conversacion['nombreReceptor']:$conversacion['nombreEmisor']?></h3>
                        <p class="textoMensaje"><?=$conversacion['texto']?></p>
                        <p class="fecha"><?=(new MomentPHP())->from(new MomentPHP($conversacion['fecha']))?></p>
                    </div>
                    <i class="fa fa-angle-right verMensajes"></i>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>