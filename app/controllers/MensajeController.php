<?php
/**
 * Created by PhpStorm.
 * User: desktop
 * Date: 12/11/17
 * Time: 19:53
 */

namespace ticket\app\controllers;

use ticket\app\entities\Mensaje;
use ticket\core\App;
use ticket\core\database\QueryBuilder;
use ticket\core\Response;

class MensajeController
{

    function mostrarConversaciones(){
        $user = App::get('user');
        $conversaciones = $this->getConversaciones($user->getId());
        Response::renderView('listarConversaciones', 'Conversaciones', ['conversaciones'=>$conversaciones], 'mensajes');
    }

    function mostrarMensajes($id){
        $user = App::get('user');
        $mensajes = $this->getMensajes($id,$user->getId());
        $contacto = App:: get ('database')->find('usuarios', 'Usuario', $id);
        Response::renderView('listarMensajes', 'Mensajes', ['mensajes'=>$mensajes, 'contacto'=>$contacto], 'mensajes');
    }

    private function getMensajes($idContacto, $idUsuario){
        $sql = "SELECT mensajes.texto as texto, mensajes.fecha as fecha, mensajes.leido as leido, mensajes.id,
                  usuEmisor.nombre as nombreEmisor, usuEmisor.id as idEmisor, usuEmisor.avatar as avatarEmisor,
                  usuReceptor.nombre as nombreReceptor, usuReceptor.id as idReceptor, usuReceptor.avatar as avatarReceptor
                FROM mensajes
                  INNER JOIN usuarios usuEmisor ON mensajes.emisor = usuEmisor.id
                  INNER JOIN usuarios usuReceptor ON mensajes.receptor = usuReceptor.id
                WHERE (mensajes.emisor = :idContacto AND mensajes.receptor = :idUsuario)
                OR (mensajes.receptor = :idContacto AND mensajes.emisor = :idUsuario)
                ORDER BY mensajes.fecha";
        $params = [
            ':idUsuario' => $idUsuario,
            ':idContacto' => $idContacto,
        ];
        return App::get('database')->customQuery($sql, $params);
    }

    private function getConversaciones($idUsuario){
        $sql = "SELECT mensajes.texto as texto, mensajes.fecha as fecha, mensajes.leido as leido, mensajes.id,
                  usuEmisor.nombre as nombreEmisor, usuEmisor.id as idEmisor, usuEmisor.avatar as avatarEmisor,
                  usuReceptor.nombre as nombreReceptor, usuReceptor.id as idReceptor, usuReceptor.avatar as avatarReceptor
                FROM mensajes
                  INNER JOIN usuarios usuEmisor ON mensajes.emisor = usuEmisor.id
                  INNER JOIN usuarios usuReceptor ON mensajes.receptor = usuReceptor.id
                WHERE (mensajes.receptor = :id)
                AND mensajes.id IN (SELECT max(id) as id FROM mensajes GROUP BY mensajes.emisor)
                ORDER BY mensajes.fecha DESC";
        $params = [
            ':id' => $idUsuario
        ];
        return App::get('database')->customQuery($sql, $params);
    }

    function enviarMensaje(){
        $user = App::get('user');
        header('Content-type: application/json');
        if(!isset($_POST['texto']) || empty(trim($_POST['texto'])) ||
            !isset($_POST['destinatario']) || empty(trim($_POST['destinatario']))){
            echo json_encode(["error"=>"No has especificado nombre o destinatario"]);
        }else{
            $datos = [
                "emisor"=>$user->getId(),
                "receptor"=>$_POST['destinatario'],
                "texto"=>$_POST['texto']
            ];
            $ok = App::get('database')->insert('mensajes', $datos);

            if($ok===true){
                echo json_encode(["error"=>"", "texto"=>$_POST['texto']]);
            }else{
                echo json_encode(["error"=>"hubo un error al enviar el mensaje"]);
            }
        }
    }

}