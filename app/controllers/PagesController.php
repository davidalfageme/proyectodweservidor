<?php

namespace ticket\app\controllers;

use ticket\core\Response;

class PagesController
{
    public function about()
    {
        Response:: renderView(
            'about',
            [
            ]
        );
    }

    public function notFound()
    {
        Response:: renderView(
            'error',
            'Pagina no encontrada',
            [
                'numero'=>'404',
                'error'=>'No se ha encontrado la página indicada'
            ]
        );
    }
}