<?php
/**
 * Created by PhpStorm.
 * User: desktop
 * Date: 20/11/17
 * Time: 16:43
 */

namespace ticket\app\controllers;


use ticket\core\App;
use ticket\core\Response;
use ticket\core\Security;

class AuthController
{
    function mostrar(){
        $user = App::get('user');
        if(!is_null($user)){
            App::get('router')->redirect('');
        }

        $error = null;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            $_SESSION['error'] = null;
        }
        Response::renderView('login', 'Entrar', ['error' => $error], 'login');

//        $error = defined($_SESSION['error'])?$_SESSION['error']:"";
//        if (!is_null($error) && defined($error)){
//            Response::renderView('login', 'Entrar', ['error' => $_SESSION['error']]);
//        }else {
//            Response::renderView('login', 'Entrar', []);
//        }
    }

//    public function login()
//    {
//        if (isset($_SESSION['error']))
//        {
//            $error = $_SESSION['error'];
//            $_SESSION['error'] = null;
//        }
//        Response::renderView('login', 'Login', ['error' => $error]);
//    }

    public function checkLogin()
    {
        if (isset($_POST['email']) && !empty($_POST['email']) &&
            isset($_POST['password']) && !empty($_POST['password'])) {

            $usuario = App::get('database')->findOneBy(
                'usuarios', 'Usuario',
                [
                    'email' => $_POST['email']
                ]);
            if (!is_null($usuario) && Security::checkPassword(
                    $_POST['password'],
                    $usuario->getSalt(),
                    $usuario->getPassword()) === true) {
                $_SESSION['usuario'] = $usuario->getId();
                App::get('router')->redirect('');
            } else {
                $_SESSION['error'] = 'El usuario y password introducidos no existen';

                App::get('router')->redirect('login');
            }
        }
        else
        {
            $_SESSION['error'] = 'Debes introducir el usuario y el password';
            App::get('router')->redirect('login');
        }
    }

    public function logout()
    {
        $_SESSION = [];
        if (isset ($_SESSION['usuario']))
        {
            $_SESSION['usuario'] = null;
            unset ($_SESSION['usuario']);
        }
        App:: get ('router')->redirect('login');
    }

    public function unauthorized()
    {
//        header ('HTTP/1.1 403 Forbidden', true, 403);
        Response:: renderView ('error','No autorizado', ['numero'=>403, 'error'=>'No tienes permisos para acceder'],'login');
    }

}