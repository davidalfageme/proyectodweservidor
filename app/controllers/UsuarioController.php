<?php
/**
 * Created by PhpStorm.
 * User: desktop
 * Date: 10/11/17
 * Time: 18:13
 */

namespace ticket\app\controllers;

use ticket\app\entities\Usuario;
use ticket\core\Response;
use ticket\core\App;
use ticket\core\Security;

class UsuarioController
{
    public function mostrar(){
        $error = null;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            $_SESSION['error'] = null;
        }

        Response::renderView('formNuevoUsuario', 'Registrate', ['error'=>$error], 'registro');
    }

    public function crearUsuario(){
        $user = new Usuario();
        $user->creaConPost();
        $this->gestionaImagenUsuario($user);
        if ($this->validarRobots($user) && $this->validar($user) === true) {
            $salt = Security::getSalt();
            $password = Security::encrypt($_POST['password'], $salt);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setRol('comprador');
            $this->save($user);
        }
    }

    public function mostrarPerfil($id = NULL){
        if($id === NULL){
            $usuario = App::get('user');
            Response::renderView('perfilUsuario', 'Perfil', ["usuarioMostrar"=>$usuario],'perfil');
        }else{

        }
    }

    public function listarUsuarios(){
        $usuarios = App::get('database')->findAll('usuarios', 'Usuario');
        Response::renderView('listarUsuarios', 'Usuarios', ['usuarios'=>$usuarios], 'usuarios');
    }

    private function validar(Usuario $user)
    {
        if (!isset($_POST['email']) || empty(trim($_POST['email'])) ||
            !isset($_POST['password']) || empty(trim($_POST['password'])))
        {
            $error = 'No puedes dejar vacío el campo email ni el campo password';
            Response:: renderView (
                'formNuevoUsuario','Error al entrar',
                [
                    'error'=>$error,
                    'user'=>$user,
                ],
                'registro'
            );

            return false;
        }

        return true;
    }

    private function validarRobots(Usuario $user){
        $recaptcha = $_POST["g-recaptcha-response"];

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => '6LfEfD8UAAAAAKv3MN4JdfgSjTetjp07HgpP2X0G',
            'response' => $recaptcha
        );
        $options = array(
            'http' => array (
                'header' =>
                    "Content-Type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);
        if ($captcha_success->success) {
            return true;
        } else {
            $error = 'No podemos confirmar de que no seas un Robot';
            Response:: renderView (
                'formNuevoUsuario', 'Error al entrrar',
                [
                    'error'=>$error,
                    'user'=>$user,
                ],
                'registro'
            );

            return false;
        }
    }

    private function save(Usuario $usuario)
    {
        try
        {
            $parameters = [
                'nombre' => $usuario->getNombre(),
                'email' => $usuario->getEmail(),
                'ciudad' => $usuario->getCiudad(),
                'password' => $usuario->getPassword(),
                'salt' => $usuario->getSalt(),
                'direccion' => $usuario->getDireccion(),
                'avatar' => $usuario->getAvatar(),
            ];

            if (is_null($usuario->getId()))
            {
                $parameters['rol'] = 'comprador';
                App::get('database')->insert('usuarios', $parameters);
            }
            else
            {
                $filters = [
                    'id' => $usuario->getId()
                ];
                App::get('database')->update('usuarios', $parameters, $filters);
            }

            App::get('router')->redirect('usuarios');
        }
        catch(\PDOException $pdoException)
        {
            if ($pdoException->getCode() === '23000')
            {
                var_dump($pdoException);
                $_SESSION['error_message'] = 'Ya existe un usuario ' . $usuario->getNombre();
//                App::get('router')->redirect('usuarios');
            }
            var_dump($pdoException);

        }
        catch(Exception $exception)
        {
            var_dump($exception);
            $error = $exception->getMessage();
        }
    }

    private function gestionaImagenUsuario(Usuario $usuario)
    {
        if($_FILES['avatar']['tmp_name']!=''){
            try
            {
                $usuario->setDirUpload(__DIR__.'/../../public/uploads/usuarios/');
                $usuario->setNombreCampoFile('avatar');
                $usuario->setTiposPermitidos(
                    [
                        'image/jpg',
                        'image/jpeg',
                        'image/gif',
                        'image/png'
                    ]);
                $usuario->setAvatar($usuario->subeImagen());
            }
            catch (UploadException $uploadException)
            {
                if ($uploadException->getFileError() === UPLOAD_ERR_NO_FILE)
                    $usuario->setFoto('/uploads/usuarios/default.png');
                else
                {
                    throw $uploadException;
                }
            }
        }

    }
}