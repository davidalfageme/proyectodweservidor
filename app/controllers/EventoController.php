<?php
/**
 * Created by PhpStorm.
 * User: desktop
 * Date: 12/11/17
 * Time: 16:46
 */

namespace ticket\app\controllers;

use function PHPSTORM_META\type;
use ticket\app\entities\Evento;
use ticket\core\app;
use ticket\core\Response;

class EventoController
{
    public function formNuevo(){
        $categorias = App::get('database')->findAll('categorias', 'Categoria');
        Response::renderView(
            'formNuevoEvento','Crear evento',
            [
                'categorias' => $categorias
            ], 'eventos');
    }

    public function crear(){
        $evento = new Evento();
        $evento->creaConPost();

        $this->gestionaImagenEvento($evento);
        $parameters = [
            'nombre' => $evento->getNombre(),
            'descripcion' => $evento->getDescripcion(),
            'fecha' => date('y-m-d',strtotime($evento->getFecha())),
            'precio' => $evento->getPrecio(),
            'ciudad' => $evento->getCiudad(),
            'direccion' => $evento->getDireccion(),
            'categoria' => $evento->getCategoria(),
            'entradasTotales' => $evento->getEntradasTotales(),
            'entradasDisponibles' => $evento->getEntradasTotales(),
            'imagen' => $evento->getImagen(),
            'gradient1' => "#".$evento->getGradient1(),
            'gradient2' => "#". $evento->getGradient2(),
            'creador' => 1,
        ];

//        var_dump($parameters);
        App::get('database')->insert('eventos', $parameters);
        App::get('router')->redirect('eventos/');
    }

    public function listar(){


        $eventos = $this->getEventos();
        $categorias = App::get('database')->findAll('categorias', 'Categoria');
        $esBusqueda = !empty($_GET);
        $menu = $esBusqueda && isset($_GET['usuario']) && $_GET['usuario'] === app::get('user')->getId()?
            'misEventos':
            'eventos';
        $eventosDestacados = array_filter($eventos, function ($ev){ return $ev->getSlider();});

        if(count($eventos)===1 && $menu !== 'misEventos'){
            $evento = $eventos[0];
            $creador = App::get('database')->find('usuarios', 'Usuario', $evento->getCreador());
            Response::renderView('detalleEvento',  $evento->getNombre() ,['evento'=> $evento, 'creador'=>$creador], 'eventos');
        }else{
            Response::renderView('mostrarEventos', 'Eventos',
                [ 'eventos' => $eventos, 'categorias' => $categorias, 'esBusqueda'=> $esBusqueda, 'destacados'  => $eventosDestacados],
                $menu
            );
        }

    }

    private function getEventos(){
        $sql = 'SELECT * FROM eventos';
        $params = [];
        $glue = ' WHERE ';
        if(isset($_GET['textoBusqueda']) && $_GET['textoBusqueda']!==""){
            $sql .=  $glue . '(nombre LIKE \'%' . $_GET['textoBusqueda']. '%\' OR  descripcion LIKE \'%' . $_GET['textoBusqueda']. '%\')';
            $glue = ' AND ';
//            $params['textoBusqueda'] = $_GET['textoBusqueda'];
        }

        if(isset($_GET['categorias'])){
            $sql .= $glue . '(eventos.categoria IN (:categorias))';
            $glue = ' AND ';
            $params['categorias'] = implode(',',$_GET['categorias']);
        }

        if(isset($_GET['usuario'])){
            $sql .= $glue . '(eventos.creador = :creador)';
            $glue = ' AND ';
            $params['creador'] = $_GET['usuario'];
        }

        $sql .= ' ORDER BY fecha;';
        return App::get('database')->customQuery($sql, $params, 'Evento');

    }

    public function mostrar($params){
        $id = $params;
        $evento = App::get('database')->find('eventos', 'Evento', $id);
        if(!$evento){
            $numero = 404;
            $error = "No se encontró el evento indicado";
            Response::renderView('error', 'Error', ['error'=> $error, 'numero' => $numero], 'eventos');
        }else{
            $creador = App::get('database')->find('usuarios', 'Usuario', $evento->getCreador());
            Response::renderView('detalleEvento',  $evento->getNombre() ,['evento'=> $evento, 'creador'=>$creador], 'eventos');
        }
    }

    private function gestionaImagenEvento(Evento $evento)
    {
        try
        {
            $evento->setDirUpload(__DIR__.'/../../public/uploads/eventos/');
            $evento->setNombreCampoFile('imagen');
            $evento->setTiposPermitidos(
                [
                    'image/jpg',
                    'image/jpeg',
                    'image/gif',
                    'image/png'
                ]);
            $evento->setImagen($evento->subeImagen());
        }
        catch (UploadException $uploadException)
        {
            if ($uploadException->getFileError() === UPLOAD_ERR_NO_FILE)
                $evento->setFoto('/uploads/eventos/default.png');
            else
            {
                throw $uploadException;
            }
        }
    }
}