<?php
/**
 * Created by PhpStorm.
 * User: desktop
 * Date: 12/11/17
 * Time: 19:54
 */

namespace ticket\app\controllers;
include(__DIR__.'/../helpers/phpqrcode/qrlib.php');

use ticket\app\entities\Entrada;
use ticket\core\App;
use ticket\core\Response;
use ticket\app\entities\Usuario;

class EntradaController
{
    public function mostrar(){
        $usuario = App::get('user');
        $idUsuario = $usuario->getId();
        $entradas = App::get('database')->getEntradas($idUsuario);
        Response::renderView('entradas', 'Mis entradas', ["entradas"=>$entradas], 'entradas');
    }

    public static function getQr($codigo){
        $usuario = App::get('user');
        \QRcode::png("Entrada: ".$codigo . " del usuario: " . $usuario->getId());
    }

    public function comprarEntrada(){
        $evento = App::get('database')->find('eventos', 'Evento', $_GET['evento']);

        if(!is_null($evento)){
            Response::renderView('comprarEntradas', 'Comprar entradas', ["evento"=>$evento], 'entradas');
        }else{
            //No ha enviado un evento
        }
    }

    public function confirmarCompra(){
        $evento =  App::get('database')->find('eventos', 'Evento', $_POST['evento']);
        $cantidadEntradas = +($_POST['cantidadEntradas']);
        $paramsFactura = [
            'cantidadEntradas'=> $cantidadEntradas,
            'comprador' => App::get('user')->getId(),
            'precioTotal' => $cantidadEntradas * $evento->getPrecio(),
            'descuento' => 0
        ];

        $idInsertado = -1;
        $ok = App::get('database')->insert('facturas', $paramsFactura, $idInsertado);

        if($ok){
            $paramsEntradas = [
                "codigo"=>"$cantidadEntradas entradas para $ $evento->getNombre()",
                "evento"=>$evento->getId(),
                "comprador"=>App::get('user')->getId(),
                "factura"=>$idInsertado,
                "precioVenta"=>$evento->getPrecio(),
                "precio"=>$evento->getPrecio()
            ];

            $ok = App::get('database')->insert('entradas', $paramsEntradas, $idInsertado);

            if($ok){
                App::get('router')->redirect('entradas/');
            }else{
                $error = 'Hubo un error durante la compra';
                Response::renderView('comprarEntradas', 'Comprar entradas', ["evento"=>$evento, "error"=>$error], 'entradas');
            }

        }else{

        }


    }
}

