window.addEventListener('load',()=>{
    const botonEnviar = document.getElementById('enviar');

    const formularioMensaje = document.getElementById('enviarMensaje');

    botonEnviar.addEventListener('click', enviarMensaje);
    formularioMensaje.addEventListener('submit', enviarMensaje);

    const historialMensajes = document.getElementById("historialMensajes");
    historialMensajes.scrollTop = historialMensajes.scrollHeight;
    function enviarMensaje(event) {
        event.preventDefault();

        //datos
        const inputTexto = document.getElementById('texto');
        const textoEnviar = inputTexto.value;
        const inputDestinatario = document.getElementById('destinatario');
        const numDestinatario = +inputDestinatario.value;

        var xhttpRequest = new XMLHttpRequest();
        var url ='/mensajes/enviar/';
        xhttpRequest.open('POST', url, true);
        xhttpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttpRequest.send( `texto=${textoEnviar}&destinatario=${numDestinatario}`);
        xhttpRequest.onreadystatechange = function (){
            if (this.status === 200 && this.readyState === 4){
                var respuesta = JSON.parse(xhttpRequest.response);

                if (respuesta.error === ""){
                    inputTexto.value = "";
                    let liMensaje = document.createElement('li');
                    liMensaje.classList.add('mensaje', 'enviadoPorMi');
                    liMensaje.innerText = respuesta.texto;
                    document.getElementById('historialMensajes').appendChild(liMensaje);
                    historialMensajes.scrollTop = historialMensajes.scrollHeight;
                }else{
                    console.error(respuesta.error);
                    formularioMensaje.classList.add('error');
                }
            }
        };
    }
})