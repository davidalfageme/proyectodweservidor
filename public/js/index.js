function bindEvent (ids) {
    ids.forEach( (id) => {
        let element = document.getElementById(id);
        console.log(element);
        if(element !== null){
            element.addEventListener('keyup', (ev)=>{
                let elementShow = document.getElementById('preview'+id);
                console.log('preview'+id);
                let value = "";

                if(ev.target.type==='text' || ev.target.type === 'date' ||ev.target.type === 'number'){
                    value = element.value;
                }else if(ev.target.type === 'textarea'){
                    value = element.value;
                    console.log('ECHO', value);
                }

                if(elementShow)
                    elementShow.innerHTML = value;
            });
        }
    });
}


function setImage(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    if (file) reader.readAsDataURL(file);
    reader.addEventListener('load', function (e) {
        document.getElementById("previewimagen").src = reader.result;
    });
}


window.addEventListener('load', ()=>{
   let btnInicioSesion = document.getElementById('iniciarSesion');
   if(btnInicioSesion){
       btnInicioSesion.addEventListener('click', () =>{
           document.getElementById('webContent').classList.add('blur');
           document.getElementById('modalLogin').classList.remove('hidden');
       });
   }

   document.getElementById('closeModal').addEventListener('click', ()=>{
       document.getElementById('webContent').classList.remove('blur');
       document.getElementById('modalLogin').classList.add('hidden');
   });


   // //OCULTAR LA BARRA CUANDO HACES SCROLL
   //  let ultimoScroll = 0;
   //  let scrollTotal = 0;
   //  document.addEventListener("scroll", ()=>{
   //      let st = window.pageYOffset || document.documentElement.scrollTop;
   //      if (st > ultimoScroll){
   //          moverNavbar(-Math.abs(st-ultimoScroll));
   //      } else {
   //          moverNavbar(Math.abs(st-ultimoScroll));
   //      }
   //      ultimoScroll = st;
   //  });
   //
   //  let navbarPosition = 44;
   //  function moverNavbar(cuanto) {
   //      let headerHeight = 44;
   //      let navbar = document.getElementsByClassName('site-nav')[0];
   //      navbarPosition += cuanto;
   //      let navbarTamano = 64;//+navbar.style.height.replace('px', '');
   //      navbarPosition = navbarPosition<-navbarTamano?-navbarTamano:navbarPosition;
   //      if(navbarPosition+cuanto+navbarTamano>0&&navbarPosition+cuanto<headerHeight)
   //          navbar.style.top = (navbarPosition + cuanto)+'px';
   //  }
});

