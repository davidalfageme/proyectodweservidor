<?php

require_once '../vendor/autoload.php';

use ticket\core\Request;
use ticket\core\Router;
use ticket\core\App;
use ticket\app\controllers\IdiomaController;


$database = require '../core/bootstrap.php';


$log = new Monolog\Logger($config['logs']['name']);
$log->pushHandler(
    new Monolog\Handler\StreamHandler(
        $config['logs']['file'],
        Monolog\Logger:: WARNING )
);

$usuario = null;

if (isset($_SESSION['usuario'])) {
    $usuario = App:: get ('database')->find(
        'usuarios', 'Usuario', $_SESSION['usuario']);
}else{
    $usuario = null;
}

App:: bind ('user', $usuario);


IdiomaController::gestionaIdiomas();

try
{
    $router = Router::load(__DIR__.'/../app/Routes.php');
    App::get('router')->direct(Request::uri(), Request::method());
}
catch(Exception $ex)
{
    $log->addError($ex->getMessage());
}