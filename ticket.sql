-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 07, 2018 at 10:11 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticket`
--
CREATE DATABASE IF NOT EXISTS `ticket` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ticket`;

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` text,
  `image` varchar(200) DEFAULT NULL,
  `icon` varchar(20) NOT NULL DEFAULT 'fa-circle-o'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `image`, `icon`) VALUES
(1, 'general', 'categoria general', '/categorias/img.jpg', 'fa-circle-o'),
(2, 'Conciertos', 'Encuentra los mejores conciertos del 2018 en cualquier ciudad de España y compra ya tus entradas. \n\nConsulta nuestro listado de conciertos y no te pierdas la próxima visita de tu grupo favorito en tu ciudad.\n\nEntérate de la gira de tu artistas preferido y no te quedes en casa por falta de planes.  ', 'sinimagen', 'fa-music'),
(3, 'Monólogos', 'Descubre los mejores monólogos de tu ciudad y compra ya tus entradas para ver a los monologuistas del momento.\r\n\r\nConsulta nuestro listado de monólogos y entérate de la programación de las salas y teatros de monólogos.\r\n\r\n¿Tienes ganas de ver en directo a David Guapo, Leo Harlem, Berto Romero o Toni Moog? No te pierdas los monólogos más divertidos y disfruta de sus actuaciones.', 'sinimagen', 'fa-microphone'),
(5, 'Deportes', 'Conoce todos los eventos deportivos disponibles en tu ciudad.\r\n\r\nDisfruta de tus estrellas del deporte favoritas y no te pierdas los partidos de Fútbol y de Baloncesto ni los torneos de Tenis y Pádel de este año.\r\n\r\nEntérate de todos los eventos y actividades relacionadas con el deporte y ¡no te quedes sin entradas!', NULL, 'fa-bicycle'),
(6, 'Festival', 'Encuentra los mejores festivales de música del momento dentro de España y compra ya tus entradas y abonos. \r\n\r\nGuarda en tus favoritos nuestro listado de festivales 2018 y no te pierdas ninguno de los eventos musicales más importantes del año.\r\n\r\nDisfruta de tus artistas favoritos en directo y sigue a tu grupo por los festivales de este año.', NULL, 'fa-hand-peace-o'),
(7, 'Hot', NULL, 'fa-heart', 'fa-heart');

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int(11) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `evento` int(11) NOT NULL,
  `comprador` int(11) NOT NULL,
  `factura` int(11) NOT NULL,
  `precioVenta` int(11) NOT NULL,
  `precio` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `codigo`, `evento`, `comprador`, `factura`, `precioVenta`, `precio`) VALUES
(1, '39123', 4, 4, 1, 200, '23'),
(2, '4x29237sidecars2018', 16, 4, 1, 22, '23'),
(3, '10 entradas para $ ()', 4, 4, 3, 5, '5'),
(4, '5 entradas para $ ()', 4, 4, 4, 5, '5');

-- --------------------------------------------------------

--
-- Table structure for table `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `gradient1` varchar(7) NOT NULL DEFAULT '#FFC107',
  `gradient2` varchar(7) NOT NULL DEFAULT '#3F51B5',
  `slider` tinyint(1) NOT NULL DEFAULT '0',
  `fecha` datetime NOT NULL,
  `inicioVentaEntradas` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `finVentaEntradas` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `precio` decimal(10,0) NOT NULL,
  `ciudad` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `entradasTotales` int(11) NOT NULL,
  `entradasDisponibles` int(11) NOT NULL,
  `creador` int(11) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eventos`
--

INSERT INTO `eventos` (`id`, `nombre`, `imagen`, `gradient1`, `gradient2`, `slider`, `fecha`, `inicioVentaEntradas`, `finVentaEntradas`, `precio`, `ciudad`, `direccion`, `descripcion`, `categoria`, `entradasTotales`, `entradasDisponibles`, `creador`, `lat`, `lng`) VALUES
(2, 'WORLD PADEL TOUR Keler Bilbao Open 2017', 'gamergy.jpg', '#FFC107', '#3F51B5', 0, '0005-02-05 00:00:00', '2018-01-08 18:36:47', '2018-01-09 18:36:47', '123', 'alicate', 'falsa 123', 'decripcion', 1, 600, 600, 1, NULL, NULL),
(3, 'Gamergy 8', 'gamergy.jpg', '#00C1E8', '#E80023', 0, '2018-12-15 00:00:00', '2018-01-07 18:36:47', '2018-01-07 18:36:47', '13', 'Madrid', 'IFEMA', 'Gamergy es el evento de eSports más importante de España. Organizado por IFEMA y Liga de Videojuegos Profesional (LVP), se celebra dos veces al año y en él se dan cita algunas de las competiciones amateurs más relevantes de nuestro país y las finales de las competiciones profesionales de League of Legends, entre otros juegos.\n\nGran fiesta de los videojuegos, Gamergy alcanza en diciembre su octava edición.', 1, 20, 20, 4, NULL, NULL),
(4, 'TEDxAlcoi2018', 'tedx.jpg', '#E62B1E', '#600C11', 1, '2001-01-01 00:00:00', '2018-01-07 18:36:47', '2018-01-07 18:36:47', '5', 'Alcoy, Alicante', 'CINES AXION, Alcoi', 'Evento TEDxAlcoi del año 2018. \n\nEvento de charlas cortas (máximo 18 minutos cada una) con temática multidisciplinar. El evento también incorpora ingredientes artísticos y actividades de networking. ', 1, 500, 500, 1, NULL, NULL),
(16, 'Concierto de Sidecars en Elche', 'sidecars.jpg', '#74033C', '#5A5578', 1, '2018-08-20 00:00:00', '2018-01-07 18:36:47', '2018-01-07 18:36:47', '22', 'Elche, Alicante', 'La Llotja, Elche', 'La banda Sidecars estará actuando en Elche el próximo sábado 20 de enero de 2018. El concierto tendrá lugar en la Sala La Llotja a las 22:00 h y la apertura de puertas será a las 21:00 h.  Inminente estreno de su nuevo single: “Tu mejor pesadilla”  Sidecars llevan trabajando el circuito de conciertos estatal desde hace ya una década, han ido creciendo con los años, superándose con cada nuevo disco, aprendiendo y siendo tan fieles a su público como este lo es con ellos.   El próximo 13 de octubre se publicará su quinto disco, un trabajo que ha sido grabado bajo la sabia batuta del productor Nigel Walker, experimentado ingeniero británico que ya ha trabajado con ellos anteriormente. El nuevo trabajo que presenta la banda madrileña a mediados de octubre seguirá marcado por esas canciones llenas de melodías pegadizas y estribillos cómplices, ese pop canalla y elegante que el grupo sabe tejer sobre los escenarios con pasmosa facilidad y efectividad.  El primer single de este nuevo disco se estrenará el 21 de Julio, ¿el título?: “Tu mejor pesadilla”. Los relojes empiezan la cuenta atrás mientras esperamos este nuevo lanzamiento de Sidecars.  Entradas para Sidecars en la Sala La Llotja el 20 de enero Las entradas para el concierto de Sidecars en la Sala La Llotja de Elche tienen un precio de lanzamiento de 17 euros + gastos de gestión. ¡No te quedes sin entradas!  Importante: prohibida la entrada a menores de 18 años.', 2, 999, 999, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facturas`
--

CREATE TABLE `facturas` (
  `id` int(11) NOT NULL,
  `cantidadEntradas` int(11) NOT NULL,
  `comprador` int(11) NOT NULL,
  `precioTotal` decimal(10,0) NOT NULL,
  `descuento` decimal(10,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facturas`
--

INSERT INTO `facturas` (`id`, `cantidadEntradas`, `comprador`, `precioTotal`, `descuento`) VALUES
(1, 2, 4, '200', '0'),
(2, 22, 4, '110', '0'),
(3, 10, 4, '50', '0'),
(4, 5, 4, '25', '0');

-- --------------------------------------------------------

--
-- Table structure for table `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `emisor` int(11) NOT NULL,
  `receptor` int(11) NOT NULL,
  `leido` tinyint(1) NOT NULL DEFAULT '0',
  `texto` text NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mensajes`
--

INSERT INTO `mensajes` (`id`, `emisor`, `receptor`, `leido`, `texto`, `fecha`) VALUES
(1, 4, 5, 1, 'Ha habido un problema con el hola caracola rubeola de torregrosa, no se encontro', '2018-01-05 00:00:00'),
(2, 5, 4, 1, 'hola', '2018-01-04 00:00:00'),
(3, 4, 5, 1, 'caracola', '2018-01-06 06:23:31'),
(4, 4, 6, 0, 'Esto es un ejemlo', '2018-01-06 15:31:36'),
(5, 6, 4, 0, 'y yo te respondo', '2018-01-05 08:25:34'),
(6, 4, 6, 0, 'hola david', '2018-01-07 01:36:03'),
(7, 4, 6, 0, 'caracola', '2018-01-07 01:36:20'),
(8, 4, 6, 0, 'david', '2018-01-07 01:36:30'),
(9, 4, 6, 0, 'mariola', '2018-01-07 01:37:01'),
(10, 4, 6, 0, 'funciona?', '2018-01-07 01:41:20'),
(11, 4, 6, 0, 'funciona?', '2018-01-07 01:41:29'),
(12, 4, 6, 0, 'parece que si', '2018-01-07 01:41:34'),
(13, 4, 6, 0, 'claro que funciona', '2018-01-07 01:43:18'),
(14, 4, 6, 0, 'rubeola', '2018-01-07 01:45:08'),
(15, 4, 6, 0, 'Caracola', '2018-01-07 01:26:33'),
(16, 4, 5, 0, 'Caracola', '2018-01-07 01:44:01'),
(17, 4, 6, 0, 'Jsjsbsjs', '2018-01-07 13:36:44'),
(18, 4, 6, 0, 'hola', '2018-01-07 20:12:14'),
(19, 4, 6, 0, 'Nsnsns', '2018-01-07 20:51:33'),
(20, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:38'),
(21, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:39'),
(22, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:40'),
(23, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:43'),
(24, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:44'),
(25, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:44'),
(26, 4, 1, 0, 'Hola soy yo', '2018-01-07 22:09:45'),
(27, 4, 1, 0, '22', '2018-01-07 22:10:31'),
(28, 4, 1, 0, 'caracola', '2018-01-07 22:10:38'),
(29, 4, 1, 0, 'mariola', '2018-01-07 22:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(22) NOT NULL,
  `rol` enum('admin','gestor','comprador','') NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `lat` int(11) DEFAULT NULL,
  `lng` int(11) DEFAULT NULL,
  `avatar` varchar(255) NOT NULL,
  `ciudad` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `password`, `salt`, `rol`, `direccion`, `lat`, `lng`, `avatar`, `ciudad`) VALUES
(1, 'david', 'alfalfageme22@gmail.com', '123', '', 'admin', 'calle falsa 123', NULL, NULL, 'default.png', 'alicante'),
(2, 'Jorge', 'jorge@jorge.es', '123', '', 'admin', 'CALLE FALSA 123', NULL, NULL, 'VIR_212856_17052_puedes_continuar_estas_canciones_de_nach.jpg', 'MURCIA'),
(3, 'david alfageme', 'alfalfageme2@gmail.com', '$2y$05$aoY0shs4po8gaETNYJfiRO15AxKJAZClCE0YqD6IJf45KVpio.RG6', 'aoY0shs4po8gaETNYJfiRS', 'comprador', 'c/ capitan dema nº9 3º, 48788930l', NULL, NULL, '1512053965_zpu-artista-rap-3.jpg', 'alicante'),
(4, 'David Alfageme', 'alfalfageme@gmail.com', '$2y$05$Ah.EabRlo.0t4SKV9QfFgu4X8FW26n7nNZMIfY.QpkiV048mlvW0u', 'Ah.EabRlo.0t4SKV9QfFgw', 'admin', 'calle la plata', NULL, NULL, 'elonmusk.jpg', 'Alicante'),
(5, 'david', 'alfalfageme222@gmail.com', '$2y$05$silViX3TYe7uyO4ewZjM4OgBDXJxILodRdiWc4ysPvnq1A5Kmpdh6', 'silViX3TYe7uyO4ewZjM4S', 'comprador', 'alfalfa', NULL, NULL, 'Prestashop-logo.png', '123'),
(6, 'perico', 'perico@perico.com', '$2y$05$n6MERmUnE1cY/urtVjrv2OtCMDKwKj3XxlYC5v/VA8KwYMCssUN2i', 'n6MERmUnE1cY/urtVjrv2Q', 'comprador', '123123', NULL, NULL, 'IMG_20170825_152449.jpg', 'Alicante');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_unique` (`nombre`);

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `CODIGO_UNIQUE` (`codigo`),
  ADD KEY `fk_evento` (`evento`),
  ADD KEY `fk_factura` (`factura`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_creador` (`creador`),
  ADD KEY `fk_categoria` (`categoria`);

--
-- Indexes for table `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comprador` (`comprador`);

--
-- Indexes for table `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_emisor` (`emisor`),
  ADD KEY `fk_receptor` (`receptor`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `entradas`
--
ALTER TABLE `entradas`
  ADD CONSTRAINT `fk_evento` FOREIGN KEY (`evento`) REFERENCES `eventos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_factura` FOREIGN KEY (`factura`) REFERENCES `facturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `fk_categoria` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_creador` FOREIGN KEY (`creador`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `facturas_ibfk_1` FOREIGN KEY (`comprador`) REFERENCES `usuarios` (`id`);

--
-- Constraints for table `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `fk_emisor` FOREIGN KEY (`emisor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_receptor` FOREIGN KEY (`receptor`) REFERENCES `usuarios` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
