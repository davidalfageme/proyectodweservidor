<?php

namespace ticket\core;

class Response
{
    public static function renderView($name, $title ,$data = array(), $menuActivo='')
    {
        extract ($data);

        ob_start ();

        $tituloPagina = $title;

        $usuario = App::get('user');

        require __DIR__ . "/../app/views/$name.view.php";

        $mainContent = ob_get_clean();

        require __DIR__ . '/../app/views/layout.view.php';
    }
}