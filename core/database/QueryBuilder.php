<?php

namespace ticket\core\database;


use PDO;
use Exception;

class QueryBuilder
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * QueryBuilder constructor.
     * @param PDO $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getPDO()
    {
        return $this->pdo;
    }

    public function findAll(string $table, string $classEntity)
    {
        $sql = "SELECT * FROM $table";
        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute();

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            "ticket\\app\\entities\\$classEntity");
    }

    public function find(string $table, string $classEntity, $id)
    {
        $sql = "SELECT * FROM $table WHERE id=:id";
        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute([
            ':id' => $id
        ]);

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        $pdoStatement->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            "ticket\\app\\entities\\$classEntity");
        return $pdoStatement->fetch();
    }

    private function getFilters(array $filters, string $letra='', bool $withLike, string $glue = 'and')
    {
        $filtersConcatenados = [];

        foreach($filters as $nombre=>$valor)
        {
            if ($withLike === false)
                $filtersConcatenados[] = $nombre . '=:'. $letra . $nombre;
            else
                $filtersConcatenados[] = $nombre . ' like :'. $letra . $nombre;
        }

        return implode(" $glue ", $filtersConcatenados);
    }


    public function findBy(string $table, string $classEntity, array $filters, $withLike = false) : array
    {
        $sql = "SELECT * FROM $table";
        if (count($filters)>0)
        {
            if ($withLike === true)
            {
                $filters = array_map(function ($valor)
                {
                    return '%' . $valor . '%';
                }, $filters);
            }

            $sql .= ' WHERE ' . $this->getFilters($filters, '', $withLike);
        }

        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute($filters);
        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            "ticket\\app\\entities\\$classEntity");
    }

    public function findOneBy(string $table, string $classEntity, array $filters, $withLike = false)
    {
        $result = $this->findBy($table, $classEntity, $filters, $withLike);

        if (count($result) > 0)
            return $result[0];

        return null;
    }

    public function insert(string $table, array $parameters, &$idInsertado=-1)
    {
        $keys = array_keys($parameters);

        $sql = sprintf(
            "INSERT INTO $table (%s) VALUES (%s)",
            implode(', ', $keys),
            ':' . implode(', :', $keys));

        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute($parameters);
        if ($res === FALSE) {
            throw new Exception('No se ha podido ejecutar la query de inserción');
            return false;
        }
        $idInsertado = $this->pdo->lastInsertId();
        return true;
    }

    public function getEntradas(string $idUsuario){
        $sql = "SELECT * FROM `entradas` 
                  INNER JOIN facturas ON entradas.factura = facturas.id 
                  INNER JOIN eventos ON entradas.evento = eventos.id 
                WHERE facturas.comprador = :id";

        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute([
            ':id' => $idUsuario
        ]);

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        $pdoStatement->setFetchMode(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            "ticket\\app\\entities\\Entrada");
        return $pdoStatement->fetchAll();
    }

    public function customQuery($sql, $params, $clase =""){

        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute($params);

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');



        if($clase!==""){
            return $pdoStatement->fetchAll(
                PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                "ticket\\app\\entities\\$clase");
        }

        return $pdoStatement->fetchAll();
    }
}